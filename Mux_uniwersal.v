`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:28:00 10/12/2017 
// Design Name: 
// Module Name:    Mux_uniwersal 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module Mux_uniwersal #(parameter SIZE=4, CNTin=4)(
    input [clogb2(CNTin)-1:0] adr,
	 input [SIZE*CNTin-1:0] value,
    output reg [SIZE-1:0] out
    );
`include "package.v"
always @*
	out <= value >> (adr*SIZE);
		
endmodule
