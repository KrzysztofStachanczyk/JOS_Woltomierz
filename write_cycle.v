`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:18:55 10/26/2017 
// Design Name: 
// Module Name:    write_cycle 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module write_cycle(
    input clk,
    input rst,
    input reg_sel,
    output reg wr_finish,
    input wr_enable,
    output reg E_out,
    output RW_out,
    output RS_out
    );

assign RS_out=reg_sel;
assign RW_out=0;

localparam idle=2'b00;
localparam init=2'b01;
localparam Eout=2'b10;
localparam endwr=2'b11;
reg [1:0] stateMachine;
reg [1:0] nst;

always@(posedge clk, posedge rst)
	if (rst)
		stateMachine <= idle;
	else
		stateMachine <= nst;
		
always @* begin
	wr_finish= 1'b0;
	E_out = 1'b0;
	case (stateMachine)
		idle:
				if (wr_enable)
					nst = init;
				else
					nst = idle;
		init:
			begin
				E_out = 1'b1;
				nst = Eout;
			end
		Eout: begin
				E_out = 1'b1;
				nst = endwr;
				end
		endwr: 
			begin
				wr_finish = 1'b1;
				nst = idle;
			end	
	endcase
	end
endmodule
