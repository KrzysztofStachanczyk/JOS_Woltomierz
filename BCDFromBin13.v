`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Krzysztof Stachanczyk
// 
// Create Date:    18:41:35 11/15/2017 
// Design Name: 
// Module Name:    BCDFromBin13 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//		Module for conversion 13 bit's NBC number into 4 digit's BCD 
//		Algorithm:
// 		http://www.eng.utah.edu/~nmcdonal/Tutorials/BCDTutorial/BCDConversion.html
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module BCDFromBin13(
    input [12:0] number,
    output reg [3:0] D0,
    output reg [3:0] D1,
    output reg [3:0] D2,
    output reg [3:0] D3
    );

   reg [28:0] shift;
	integer i;
   
   always @(number)
   begin

      shift[28:13] = 0;
      shift[12:0] = number;
      
		for (i=0; i<13; i=i+1) begin
         if (shift[16:13] >= 5)
            shift[16:13] = shift[16:13] + 3;
            
         if (shift[20:17] >= 5)
            shift[20:17] = shift[20:17] + 3;
            
         if (shift[24:21] >= 5)
            shift[24:21] = shift[24:21] + 3;
				
			if (shift[28:25] >= 5)
            shift[28:25] = shift[28:25] + 3;

         shift = shift << 1;
      end
  
	  D3     = shift[28:25];
      D2     = shift[24:21];
      D1     = shift[20:17];
      D0     = shift[16:13];
		
   end
 
endmodule
