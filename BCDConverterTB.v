`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Krzysztof Stachanczyk
//
// Create Date:   19:58:55 11/15/2017
// Design Name:   BCDFromBin13
// Module Name:   /home/krzysztof/voltomierz/BCDConverterTB.v
// Project Name:  voltomierz
// Target Device:  
// Tool versions:  
// Description: 
//		Test for BCDFromBin13 run and read report from console
// Verilog Test Fixture created by ISE for module: BCDFromBin13
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module BCDConverterTB;

	// Inputs
	reg [12:0] number;

	// Outputs
	wire [3:0] D0;
	wire [3:0] D1;
	wire [3:0] D2;
	wire [3:0] D3;

	integer i;
	integer delta;
	integer mistakes;
	// Instantiate the Unit Under Test (UUT)
	BCDFromBin13 uut (
		.number(number), 
		.D0(D0), 
		.D1(D1), 
		.D2(D2), 
		.D3(D3)
	);

	initial begin
		// Initialize Inputs
		number = 0;
		mistakes = 0;
		
		// Wait 100 ns for global reset to finish
		#100;
      
		$display("Test begin:");
		
		for(i=0;i<=8192;i=i+1)
		begin
			#10;
			number=number+1;
			#5;
			delta=(D0+D1*10+D2*100+D3*1000)-number;

			if(delta!=0)
			begin
				$display("Error");
				mistakes=mistakes+1;
			end
		end
		
		$display("Test end:");
		$display("\tin range [0,",i-1,"] found: ",mistakes," mistakes");
		
		$finish;
		// Add stimulus here

	end
      
endmodule

