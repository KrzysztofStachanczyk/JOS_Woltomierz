`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:46:47 12/02/2017 
// Design Name: 
// Module Name:    AmpGainRotor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module AmpGainRotor(
	input rotorA,
	input rotorB,
    input clk,
    input rst,
    output reg gainChanged,
    output reg [3:0] gain
    );

wire [2:0] curGain;

ClockDivider  #(.div(10000)) clkDiv(.clk(clk),.rst(rst),.clk_slow(clk_slow));
SquareDecoder #(.cntBits(3)) sd(.A(rotorA),.B(rotorB),.clk(clk_slow),.rst(rst),.out(curGain));

always @(posedge clk,posedge rst)
begin
	if(rst)
		begin
			gain <= 4'b0001;
			gainChanged <= 1'b0;
		end
	else
		begin
			if(gain!={1'b0,curGain})
				begin
					gainChanged<=1'b1;
					gain<={1'b0,curGain};
				end
			else
				gainChanged<=1'b0;
		end
end

endmodule
