`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Krzysztof Stachanczyk
// 
// Create Date:    14:12:33 11/16/2017 
// Design Name: 
// Module Name:    U2ToZM 
// Project Name: 
// Target Devices:  
// Tool versions: 
// Description: 
//		Module responsible for conversion of U2_LEN bits U2(two's complement) encoded number into
//		ZM format
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module  U2ToZM #(parameter U2_LEN=14)(u2,m,s);

input [U2_LEN-1:0] u2;
output reg [U2_LEN-2:0] m;
output reg s;

always @*
begin
	s = u2[U2_LEN-1];
	if(s==1'b1)
		begin
			m = ~u2[U2_LEN-2:0];
			m = m + 1;
		end
	else
		begin
			m = u2[U2_LEN-2:0];
		end
	
end
endmodule
