`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:31:33 11/15/2017 
// Design Name: 
// Module Name:    PosedgeDetector 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module PosedgeDetector(
    input clk,
    input in,
    output reg out,
    input rst
    );

localparam s0 = 0;
localparam s1 = 1;
localparam s2 = 2;

reg [1:0] st;
reg [1:0] nst;

always @(posedge clk,posedge rst)
begin 
	if(rst)
		st <= in?s2:s0;
	else
		st <= nst;
end

always @*
begin
	nst = s0;
	case(st)
		s0: nst = in?s1:s0;
		s1: nst = in?s2:s0;
		s2: nst = in?s2:s0;
	endcase
end

always @*
begin 
	out = st==s1?1:0;
end

endmodule
