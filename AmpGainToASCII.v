`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:18:12 12/02/2017 
// Design Name: 
// Module Name:    AmpGainToASCII 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module AmpGainToASCII(
	input[3:0] gain,
	output reg [7:0] hundreds,
	output reg [7:0] tens,
	output reg [7:0] ones,
	output reg [7:0] sign
    );


// ones
always @*
begin
	sign = 8'd43;
	ones = 0;
	case(gain)
		4'b0001: ones = 8'd1;
		4'b0010: ones = 8'd2;
		4'b0011: ones = 8'd5;
	endcase
	
	ones = 8'd48+ones;
end

// tens
always @*
begin
	tens = 0;
	
	case(gain)
		4'b0100: tens = 8'd1;
		4'b0101: tens = 8'd2;
		4'b0110: tens = 8'd5;
	endcase
	
	tens = 8'd48+tens;
end

// hundreds
always @*
begin
	hundreds = 0;
	
	if(gain==4'b0111)
		hundreds=8'b1;
	
	hundreds = 8'd48+hundreds;
end

endmodule
