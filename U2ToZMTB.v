`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Krzysztof Stachanczyk
//
// Create Date:   14:18:12 11/16/2017
// Design Name:   U2ToZM
// Module Name:   /home/krzysztof/voltomierz/U2ToZMTB.v
// Project Name:  voltomierz
// Target Device:  
// Tool versions:  
// Description: 
//		Simple test for U2ToZMTB module check
// Verilog Test Fixture created by ISE for module: U2ToZM
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module U2ToZMTB;
	// Inputs
	reg [3:0] u2;

	// Outputs
	wire [2:0] m;
	wire s;

	integer mistakes;
	// Instantiate the Unit Under Test (UUT)
	U2ToZM #(.U2_LEN(4)) uut (
		.u2(u2), 
		.m(m), 
		.s(s)
	);
	
	initial begin
		// Initialize Inputs
		u2 = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

		u2 = 4'b1111;
		for(;u2>4'b0000;)
		begin
			u2 = u2 - 1; 
			#30;
		end
		
		$finish;
	end
      
endmodule

