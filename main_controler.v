`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:15:34 10/26/2017 
// Design Name: 
// Module Name:    main_controler 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module main_controler #(parameter CNTin=5)(
    input clk,
    input rst,
    output reg data_sel,
    output reg db_sel,
    output reg lcd_enable,
    output reg [clogb2(CNTin)-1:0] lcd_cnt,
    output reg mode,
    input lcd_finish,
    output reg reg_sel
    );
`include "package.v"
localparam idle=3'b000;
localparam init=3'b001;
localparam addr=3'b010;
localparam addr1=3'b011;
localparam ref=3'b100;
localparam ref1=3'b101;
reg [2:0] stateMachine;
reg [2:0] nst;

localparam LCD_INIT=1;
localparam LCD_REF=0;
localparam INIT_CONST_NO=4;
localparam REF_DATA_NO=CNTin;

always@(posedge clk, posedge rst)
	if (rst)
		stateMachine <= idle;
	else
		stateMachine <= nst;

always@* begin
	lcd_enable=0;
	lcd_cnt=0;
	db_sel=1;
	data_sel=0;
	reg_sel=0;
	mode=LCD_INIT;
	case (stateMachine)
		idle:
			begin
				lcd_enable=1;
				lcd_cnt=INIT_CONST_NO-1;
				nst = init;
			end
		init:
			begin
				if(lcd_finish)
					nst = addr;
				else
					nst = init;
			end
		addr:
			begin
				db_sel = 0;
				lcd_enable=1;
				nst = addr1;
			end
		addr1:
			begin
				db_sel = 0;
				if(lcd_finish)
					nst = ref;
				else
					nst = addr1;
			end
		
		ref:
			begin
				lcd_enable=1;
				lcd_cnt=REF_DATA_NO-1;
				data_sel=1;
				reg_sel=1;
				mode=LCD_REF;
				nst = ref1;
			end
		ref1:
			begin
				data_sel=1;
				reg_sel=1;
				mode=LCD_REF;
				if(lcd_finish)
						nst = addr;
				else
					nst = ref1;
			end	
	endcase
	end
endmodule
