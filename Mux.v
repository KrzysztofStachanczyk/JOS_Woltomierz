`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:28:00 10/12/2017 
// Design Name: 
// Module Name:    Mux 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Mux #(parameter SIZE=4)(
    input [1:0] adr,
	 
	 input [SIZE-1:0] value0,
	 input [SIZE-1:0] value1,
	 input [SIZE-1:0] value2,
	 input [SIZE-1:0] value3,
    output reg [SIZE-1:0] out
    );

always @*
	case(adr)
		0: out<= value0;
		1: out<= value1;
		2: out<= value2;
		3: out<= value3;
	endcase
endmodule
