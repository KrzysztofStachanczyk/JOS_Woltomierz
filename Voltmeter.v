`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Krzysztof Stachanczyk 
// 
// Create Date:    12:38:06 11/23/2017 
// Design Name: 
// Module Name:    Voltmeter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Voltmeter  #(parameter div = 50000, CNTin=(16+24+16))(
	output E,
   output RW,
   output RS,
	
	output SPI_SCK,
	output SPI_MOSI,
	output AMP_CS,
	output AD_CONV,
	
	output [7:0] leds,
	output [7:0] db_out,
	input rst,
	input clk,
	input ADC_OUT,
	output AMP_SHDN,
	
	input ROT_A,
	input ROT_B
);

wire [7:0] SignA, OnesA, TensA, HundredsA, ThousandsA;
wire [7:0] SignB, OnesB, TensB, HundredsB, ThousandsB;
wire [7:0] D0A, D1A, D2A, D3A;
wire [7:0] D0B, D1B, D2B, D3B;
wire [13:0] chA;
wire [13:0] chB;

wire [7:0] SignCommonGain, OnesCommonGain, TensCommonGain, HundredsCommonGain;
wire [3:0] commonChGain;

MeasUnit mu(.clk(clk),.rst(rst),.chAFiltered(chA),.chBFiltered(chB),
		 .SPI_SCK(SPI_SCK), .SPI_MOSI(SPI_MOSI),.AMP_CS(AMP_CS),
		 .AD_CONV(AD_CONV), .ADC_OUT(ADC_OUT), .AMP_SHDN(AMP_SHDN),
		 .rotorB(ROT_B),.rotorA(ROT_A),.commonChGain(commonChGain)
    );

assign leds = chA[13:6];

AmpGainToASCII gainToASCII(
	.gain(commonChGain),
	.hundreds(HundredsCommonGain),
	.tens(TensCommonGain),
	.ones(OnesCommonGain),
	.sign(SignCommonGain)
);
	 
ChannelToASCII cToASCIIA(.channel(chA),.Sign(SignA),.Ones(D0A),.Tens(D1A),.Hundreds(D2A),.Thousands(D3A));
CutZero cutA (.D0(D0A),.D1(D1A),.D2(D2A),.D3(D3A),.Ones(OnesA),.Tens(TensA),.Hundreds(HundredsA),.Thousands(ThousandsA));

ChannelToASCII cToASCIIB(.channel(chB),.Sign(SignB),.Ones(D0B),.Tens(D1B),.Hundreds(D2B),.Thousands(D3B));
CutZero cutB (.D0(D0B),.D1(D1B),.D2(D2B),.D3(D3B),.Ones(OnesB),.Tens(TensB),.Hundreds(HundredsB),.Thousands(ThousandsB));

LCD_driver #(.div(div),.CNTin(CNTin)) lcd(.E(E),.RW(RW),.RS(RS),.db_out(db_out),.clk(clk),.rst(rst),.position(8'h80),
		.value({SignA, ThousandsA, HundredsA, TensA, OnesA, " CHANNEL A ",{24{" "}}, 
					SignB, ThousandsB, HundredsB, TensB, OnesB, " CHB G:",SignCommonGain,HundredsCommonGain,TensCommonGain,OnesCommonGain}));



endmodule
