`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:17:32 10/26/2017 
// Design Name: 
// Module Name:    lcd_init_refresh 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module lcd_init_refresh #(parameter CNTin=5)(
    input clk,
    input rst,
    input lcd_enable,
    input [clogb2(CNTin)-1:0] lcd_cnt,
    input mode,
    output reg lcd_finish,
    output reg [1:0] init_sel,
    output reg [clogb2(CNTin)-1:0] mux_sel,
    output reg wr_enable,
    input wr_finish
    );
`include "package.v"

localparam idle=2'b00;
localparam data=2'b01;
localparam data1=2'b10;
localparam endlcd=2'b11;
reg [1:0] stateMachine;
reg [1:0] nst;

always@(posedge clk, posedge rst)
	if (rst)
		stateMachine <= idle;
	else
		stateMachine <= nst;

always@(posedge clk, posedge rst)
	if(rst)
		init_sel <= 2'b0;
	else case(stateMachine)
			idle: if(mode == 1'b1)
							init_sel <= lcd_cnt;
			endlcd: if(mode == 1'b1)
						if(init_sel != 2'b0)
								init_sel <= init_sel -1;
		endcase
		
always@(posedge clk, posedge rst)
	if(rst)
		mux_sel <= 3'b0;
	else case(stateMachine)
			idle: if(mode == 1'b0)
							mux_sel <= lcd_cnt;
			endlcd: if(mode == 1'b0)
						if(mux_sel != 3'b0)
								mux_sel <= mux_sel -1;
		endcase
		
always@* begin
	lcd_finish=0;
	wr_enable =0;
	case (stateMachine)
		idle:
			begin
				if (lcd_enable)
					nst = data;	
				else
					nst = idle;
			end
		data:
			begin
				wr_enable =1;
				nst = data1;
			end
		data1: 
			begin
				if(wr_finish)
					nst = endlcd;
				else
					nst = data1;
			end
		endlcd: 
			begin
				if(mode)
					begin
						if(init_sel)
							begin
								nst = data;
							end
						else
							begin
								lcd_finish=1;
								nst = idle;
							end
					end
				else
					begin
						if(mux_sel)
							begin
								nst = data;
							end
						else
							begin
								lcd_finish=1;
								nst = idle;
							end
					end
			end	
	endcase
	end
endmodule
