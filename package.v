`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:22:52 11/29/2017 
// Design Name: 
// Module Name:    package 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

function integer clogb2(input integer value);
	begin
		value = value - 1;
			for (clogb2=0; value>0 ; clogb2 = clogb2 +1)
				value = value >> 1;
	end
endfunction
