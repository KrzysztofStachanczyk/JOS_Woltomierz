`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:12:37 11/29/2017
// Design Name:   LTC1407Driver
// Module Name:   /home/krzysztof/JOS_Woltomierz/LTC1407TB.v
// Project Name:  woltomierz
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: LTC1407Driver
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module LTC1407TB;

	// Inputs
	reg clk;
	reg rst;
	reg en;
	reg miso;

	// Outputs
	wire finish;
	wire sclk;
	wire conv;
	wire [13:0] chA;
	wire [13:0] chB;

	// Instantiate the Unit Under Test (UUT)
	LTC1407Driver uut (
		.clk(clk), 
		.rst(rst), 
		.finish(finish), 
		.en(en), 
		.sclk(sclk), 
		.miso(miso), 
		.conv(conv), 
		.chA(chA), 
		.chB(chB)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;
		en = 0;
		miso = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		rst = 1;
		#20;
		rst = 0;
		#100;
		
		en = 1;
		#20;
		en = 0;
		miso = 1;
		#10000;
		$finish;
	end
	
	always #10 clk = ~clk;
      
endmodule

