`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Krzysztof Stachanczyk
// 
// Create Date:    15:37:58 11/18/2017 
// Design Name: 
// Module Name:    ChannelToASCII 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//		Module for decoding 14 bit's ADC channel value into ASCII 
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module CutZero(
	 input [7:0] D0,
	 input [7:0] D1,
	 input [7:0] D2,
	 input [7:0] D3,
	 output reg [7:0] Ones,
	 output reg [7:0] Tens,
	 output reg [7:0] Hundreds,
	 output reg [7:0] Thousands
    );

always @*
begin
	Ones = D0;
	Tens = D1;
	Hundreds = D2;
	Thousands = D3;
	if(Thousands==8'd48) begin
		Thousands = 8'd32;
		if(Hundreds==8'd48) begin
			Hundreds = 8'd32;
			if(Tens==8'd48)
				Tens = 8'd32;
		end
	end
end

endmodule
