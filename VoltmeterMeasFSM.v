`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Krzysztof Stachanczyk 
// 
// Create Date:    10:06:35 11/28/2017 
// Design Name: 
// Module Name:    VoltmeterMeasFSM 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module VoltmeterMeasFSM #(parameter measureDelay=5)(
	input clk,
	input rst,
	
	input amplifierFinish,
	input adcReadReady,
	
	input updateAmplifierConf,
	
	output reg trigADCRead,
	output reg trigAmplifierWrite
    );
	 
`include "package.v"

// state machine registers
localparam AMPLIFIER_INIT = 0;
localparam AMPLIFIER_TRANSMITION = 1;

localparam ADC_READ_INIT = 2;
localparam ADC_READ_TRANSMITION = 3;

localparam DELAY = 4; 

localparam DELAY_BEFORE_START = 5;

reg [3:0] st;
reg [3:0] nst;
reg updateAmplifierBuffer;

// dalay counter 
localparam delayCntBits = clogb2(measureDelay);
reg [delayCntBits:0] delayCnt;
assign delayMatch = (delayCnt == measureDelay); 


always @(posedge clk,posedge rst)
begin
	if(rst)
		updateAmplifierBuffer=1'b0;
	else
		begin 
			if(updateAmplifierConf)
				updateAmplifierBuffer=1'b1;
			else if(st==AMPLIFIER_INIT)
				updateAmplifierBuffer=1'b0;
		end
	
end

always @(posedge clk,posedge rst)
begin
	if(rst)
		begin
		st<= DELAY_BEFORE_START;
		end
	else
		st<=nst;
end

// Next state logic 
always @*
begin
	nst = DELAY_BEFORE_START;
	case(st)
		AMPLIFIER_INIT: nst = AMPLIFIER_TRANSMITION;
		AMPLIFIER_TRANSMITION: nst= amplifierFinish?ADC_READ_INIT:AMPLIFIER_TRANSMITION;
		
		ADC_READ_INIT: nst=ADC_READ_TRANSMITION;
		ADC_READ_TRANSMITION: nst = adcReadReady?(updateAmplifierBuffer?AMPLIFIER_INIT:DELAY):ADC_READ_TRANSMITION;
		
		DELAY_BEFORE_START: nst=(delayMatch?AMPLIFIER_INIT:DELAY_BEFORE_START);
		DELAY: nst = updateAmplifierBuffer?AMPLIFIER_INIT:(delayMatch?ADC_READ_INIT:DELAY);
	endcase
end

// Counter logic
always @(posedge clk,posedge rst)
begin
	if(rst)
		delayCnt<=0;
	else
		begin
			if(st==DELAY || st==DELAY_BEFORE_START)
				delayCnt<=delayCnt+1;
			else
				delayCnt<=0;
		end
//	if(st==DELAY)
//	else
//		delayCnt=0;
end


// Outputs logic
always @*
begin
	trigADCRead=1'b0;
	trigAmplifierWrite=1'b0;
	
	if(st==AMPLIFIER_INIT)
		trigAmplifierWrite=1'b1;
	
	if(st==ADC_READ_INIT)
		trigADCRead=1'b1;
end

endmodule
