`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Krzysztof Stachanczyk
//
// Create Date:   17:34:35 11/18/2017
// Design Name:   LTC6912Driver
// Module Name:   /home/krzysztof/Pulpit/JOS_Woltomierz/LTC6912TB.v
// Project Name:  woltomierz
// Target Device:  
// Tool versions:  
// Description: 
//		
//
// Verilog Test Fixture created by ISE for module: LTC6912Driver
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module LTC6912TB;

	// Inputs
	reg clk;
	reg rst;
	reg en;
	reg clr_ctrl;
	reg [3:0] chAGain;
	reg [3:0] chBGain;

	// Outputs
	wire sclk;
	wire mosi;
	wire ss;
	wire finish;

	// Instantiate the Unit Under Test (UUT)
	LTC6912Driver uut (
		.clk(clk), 
		.rst(rst), 
		.en(en), 
		.clr_ctrl(clr_ctrl), 
		.chAGain(chAGain), 
		.chBGain(chBGain), 
		.sclk(sclk), 
		.mosi(mosi), 
		.ss(ss), 
		.finish(finish)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;
		en = 0;
		clr_ctrl = 1;
		chAGain = 0;
		chBGain = 0;

		// Wait 100 ns for global reset to finish
		#100;
      
		#25
		rst = 1'b1;
		#25;
		rst = 1'b0;
		
		#100;
		chAGain=4'b0001;
		chBGain=4'b0001;
		#55;
		en=1'b1;
		#25;
		en=1'b0;
		
		#5500;
		
		#100;
		chAGain=4'b1110;
		chBGain=4'b0010;
		#50;
		en=1'b1;
		#20;
		en=1'b0;
		#2500;

		$finish;
		
		// Add stimulus here

	end
    
	always #10 clk=~clk;
endmodule

