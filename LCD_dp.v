`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:40:41 10/26/2017 
// Design Name: 
// Module Name:    LCD_dp 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module LCD_dp #(parameter SIZE=8, CNTin=5) (
	 input [clogb2(CNTin)-1:0] mux_sel,
	 input [SIZE*CNTin-1:0] value,
    input [1:0] init_sel,
    input data_sel,
    input db_sel,
	 input [7:0] position,
    output [7:0] db_out
    );
`include "package.v"

wire [5:0] init_reset_mux;
wire [7:0] counter_mux;
wire [7:0] data_out;

Mux_uniwersal #(.SIZE(6), .CNTin(4)) mx1(.adr(init_sel), .value({6'b111000, 6'b000110, 6'b001110, 6'b000001}), .out(init_reset_mux));
Mux_uniwersal #(.SIZE(8), .CNTin(CNTin)) mx2(.adr(mux_sel), .value(value), .out(counter_mux));
Mux_uniwersal #(.SIZE(8), .CNTin(2)) mx3(.adr(data_sel), .value({counter_mux, 2'b00, init_reset_mux}), .out(data_out));
Mux_uniwersal #(.SIZE(8), .CNTin(2)) mx4(.adr(db_sel), .value({data_out, position}), .out(db_out)); //8'hc1

endmodule
