`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Krzysztof Stachanczyk
// 
// Create Date:    15:37:58 11/18/2017 
// Design Name: 
// Module Name:    ChannelToASCII 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//		Module for decoding 14 bit's ADC channel value into ASCII 
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ChannelToASCII(
	input [13:0] channel,
	output reg [7:0] Sign,
	output  [7:0] Ones,
	output  [7:0] Tens,
	output  [7:0] Hundreds,
	output  [7:0] Thousands
    );
	 
wire [12:0]m;
wire [3:0]D0;
wire [3:0]D1;
wire [3:0]D2;
wire [3:0]D3;
	 
U2ToZM #(.U2_LEN(14)) toZM(.u2(channel),.m(m),.s(s));
BCDFromBin13 toBCD (.number(m),.D0(D0),.D1(D1),.D2(D2),.D3(D3));

BCDtoASCII d0ToASCII (.in(D0),.out(Ones));
BCDtoASCII d1ToASCII (.in(D1),.out(Tens));
BCDtoASCII d2ToASCII (.in(D2),.out(Hundreds));
BCDtoASCII d3ToASCII (.in(D3),.out(Thousands));

always @*
begin
	if(s==1'b1)
		Sign = 8'd45;
	else
		Sign = 8'd43;
end

endmodule
