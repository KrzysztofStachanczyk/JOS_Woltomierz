`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Krzysztof Stachanczyk 
// 
// Create Date:    10:35:45 11/28/2017 
// Design Name: 
// Module Name:    IIRFilter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module IIRFilter #(parameter filterCoefficientPow2=4,parameter dataSize=8)(
	input [dataSize-1:0]in,
	output reg [dataSize-1:0]out,
	input clk,
	input newData,
	input rst
);

// Filter buffer
reg [filterCoefficientPow2+dataSize-1:0] filterWorkingBuffer;



// Simple FSM for resolving unitialize filter issue
reg st;
reg nst;

localparam UNINITIALIZED=0;
localparam WORKING=1;

always @(posedge clk,posedge rst)
begin
	if(rst)
		st<= UNINITIALIZED;
	else
		st<= nst;
end

// Next state logic
always @*
begin
	nst = UNINITIALIZED;
	case(st)
		UNINITIALIZED:nst=newData?WORKING:UNINITIALIZED;
		WORKING:nst=WORKING;
	endcase
end

// Filter logic
always @(posedge clk,posedge rst)
begin	
	if(rst)
	begin
		filterWorkingBuffer= 0;
		out=0;
	end
	else if(newData)
		begin
			if(st==UNINITIALIZED)
			begin
				filterWorkingBuffer=in<<<filterCoefficientPow2;
			end
			else
			begin
				if (filterWorkingBuffer[filterCoefficientPow2+dataSize-1]==1'b1)
				begin
					filterWorkingBuffer=(~filterWorkingBuffer)+1;
					filterWorkingBuffer=filterWorkingBuffer- (filterWorkingBuffer>>filterCoefficientPow2);
					filterWorkingBuffer=(~filterWorkingBuffer)+1;
				end
				else 
					filterWorkingBuffer=filterWorkingBuffer- (filterWorkingBuffer>>filterCoefficientPow2);
					
				filterWorkingBuffer=filterWorkingBuffer + {{(filterCoefficientPow2){in[dataSize-1]}},in};
				
				//filterWorkingBuffer=filterWorkingBuffer+{{(filterCoefficientPow2+1){in[dataSize-1]}},in};
			end
			out=filterWorkingBuffer[filterCoefficientPow2+dataSize-1:filterCoefficientPow2];
		end
end

endmodule
