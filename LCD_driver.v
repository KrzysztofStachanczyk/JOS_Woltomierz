`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:26:19 10/26/2017 
// Design Name: 
// Module Name:    LCD_driver 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module LCD_driver #(parameter div = 50000, SIZE=8, CNTin=5)(
    input clk,
    input rst,
	 
	 // characters ASCII
	 input [SIZE*CNTin-1:0] value,
	 input [7:0] position,
	 
    output E,
    output RW,
    output RS,
    output [7:0] db_out
    );
`include "package.v"
wire data_sel;
wire db_sel;
wire lcd_enable;
wire [clogb2(CNTin)-1:0] lcd_cnt;
wire mode;
wire lcd_finish;
wire reg_sel;
wire [1:0] init_sel;
wire [clogb2(CNTin)-1:0] mux_sel;
wire wr_finish;
wire wr_enable;

ClockDivider  #(.div(div)) clkDiv(.clk(clk),.rst(rst),.clk_slow(clk_slow));

main_controler #(.CNTin(CNTin)) mc(.clk(clk_slow), .rst(rst),.data_sel(data_sel),.db_sel(db_sel),.lcd_enable(lcd_enable),
    .lcd_cnt(lcd_cnt),
    .mode(mode),
    .lcd_finish(lcd_finish),
    .reg_sel(reg_sel));
	 
	 
lcd_init_refresh #(.CNTin(CNTin)) lcd_in (.clk(clk_slow), .rst(rst),.lcd_enable(lcd_enable),
	.lcd_cnt(lcd_cnt),.mode(mode),.lcd_finish(lcd_finish),.init_sel(init_sel),
	.mux_sel(mux_sel),.wr_enable(wr_enable),.wr_finish(wr_finish));
	
	
write_cycle wr (.clk(clk_slow), .rst(rst),.reg_sel(reg_sel),.wr_finish(wr_finish),
	.wr_enable(wr_enable),.RW_out(RW),.RS_out(RS),.E_out(E));


LCD_dp #(.CNTin(CNTin)) dp (.value(value),.init_sel(init_sel),.mux_sel(mux_sel),
.data_sel(data_sel),.db_sel(db_sel),.position(position),.db_out(db_out));
 

endmodule
