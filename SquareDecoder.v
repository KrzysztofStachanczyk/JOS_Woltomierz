`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:01:21 11/15/2017 
// Design Name: 
// Module Name:    SquareDecoder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module  SquareDecoder #(parameter cntBits = 8)(
    input A,
    input B,
    input clk,
    input rst,
    output [cntBits-1:0] out
    );
	 
Debouncer #(.len(100)) debA(.in(A),.clk(clk),.rst(rst),.out(Adeb));
Debouncer #(.len(100)) debB(.in(B),.clk(clk),.rst(rst),.out(Bdeb));

PosedgeDetector ped (.in(Adeb),.rst(rst),.clk(clk),.out(edgeDet));
MultiDirCounter #(.bits(cntBits))counter(.clk(clk),.rst(rst),.up(~Bdeb),.en(edgeDet),.out(out));



endmodule
