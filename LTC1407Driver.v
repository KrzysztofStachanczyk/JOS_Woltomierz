`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:25:05 11/28/2017 
// Design Name: 
// Module Name:    LTC1407Driver 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module LTC1407Driver( 
    input clk,
    input rst,
	 
    output reg finish,
	 input en,
	 
	 output sclk,
    input miso,
    output reg conv,
	 
    output reg[13:0] chA,
    output reg[13:0] chB
	 );

`include "package.v"

//Parametry czasu trwania:
//m - czas jednego bitu (w połowie zbocze opadające sclk)
//d - opóźnienia na początku
//Parametry obliczane:
//bm - rozmiar licznika czasu
localparam m = 3, d = 2, bm = clogb2(m);

//kodowanie stanów
localparam idle = 3'b000, shdown = 3'b001, progr = 3'b010, start = 3'b011, finish_report = 4'b100;

reg [2:0] st, nst;
reg [bm-1:0] cnt; //licznik czasu trwania stanów
reg [5:0] dcnt; //licznik bitów transmitowanych
reg tmp, cnten;

reg[13:0] chABuf;
reg[13:0] chBBuf; 
//rejestr stanu
always @(posedge clk, posedge rst)
	if(rst)
		st <= idle;
	else
		st <= nst;
		
//logika automatu
always @* begin
	nst = idle;
	cnten = 1'b1;
	
	case(st)
		idle: 
			begin
				cnten = 1'b0; 
				nst = en?start:idle;
			end
	
		shdown: nst = (cnt == m-1)?start:shdown;
		start: nst = (cnt == d)?progr:start;
		progr: nst = (dcnt == 6'b0)?finish_report:progr;
		finish_report: nst = idle;
	endcase
end

//licznik czasu trwania stanów
//i poziomów zegara transmisji
always @(posedge clk, posedge rst)
	if(rst)
		cnt <= {bm{1'b0}};
	else if(cnten)
		if(cnt == m | dcnt == 6'b0) cnt <= {bm{1'b0}};
	else 
		cnt <= cnt + 1'b1;

// jezeli w stanie prog i w pierwszej polowie cyklu
assign sclk = ((st == progr) & (cnt < (m/2 + 1)))?1'b1:1'b0;

//generator zezwolenie dla sygnalu conv
always @(posedge clk, posedge rst)
	if(rst)
		tmp<= 1'b0;
	else
		tmp <= sclk;
assign spi_en = ~sclk & tmp;

//logika sygnalu conv i rejestru odczytu
always @(posedge clk, posedge rst)	
	if(rst)
		begin
			chABuf<=0;
			chBBuf<=0;
			conv<=1'b0;
		end
	else if(en)
		begin
			chABuf<=0;
			chBBuf<=0;
			conv <= 1'b1;
		end
	else if(spi_en)
		begin
			if(dcnt>=19 & dcnt<=32)
				begin
					chABuf<={chABuf[12:0],miso};
				end
			else if(dcnt>=3 & dcnt<=16)
				begin
					chBBuf<={chBBuf[12:0],miso};
				end
			conv <= 1'b0;
			
		end

//licznik bitów
always @(posedge clk, posedge rst)
	if(rst)
		dcnt <= 34;
	else if(spi_en)
		dcnt <= dcnt - 1'b1;
	else
		if(en & dcnt == 6'b0) dcnt <= 34;

//generator sygnału gotowości
always @* begin
	finish=1'b0;
	if(st==finish_report)
		begin
			finish=1'b1;
		end
end

always @(posedge clk,posedge rst) 
begin
	if(rst)
		begin
			chA<=0;
			chB<=0;
		end
	else
		begin
			if(st==finish_report)
				begin
					chA<=chABuf;
					chB<=chBBuf;
				end
		end
end

endmodule
