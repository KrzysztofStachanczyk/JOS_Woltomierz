`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:59:44 11/15/2017 
// Design Name: 
// Module Name:    Debouncer 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Debouncer #(parameter len=5)(
    input in,
    output reg out,
    input clk,
    input rst
    );
	 
	 reg [len-1:0] shiftRegister;
	 
	 always @(posedge clk,posedge rst)
	 begin
		if(rst)
			shiftRegister <= 0;
		else
			shiftRegister <= (shiftRegister << 1)|in;
	 end
	 
	 always @(posedge clk)
	 begin
		if(&shiftRegister | ~(|shiftRegister))
			out <= shiftRegister[len-1];
	 end



endmodule
