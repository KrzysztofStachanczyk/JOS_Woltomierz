`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:50:54 10/26/2017 
// Design Name: 
// Module Name:    Mux2 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Mux2 #(parameter SIZE=4)(
    input adr,
    input [SIZE-1:0]value0,
    input [SIZE-1:0]value1,
    output reg [SIZE-1:0] out
    );

always @*
	case(adr)
		0: out <= value0;
		1: out <= value1;
	endcase
endmodule


