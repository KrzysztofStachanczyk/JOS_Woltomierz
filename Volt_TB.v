`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:05:20 11/23/2017
// Design Name:   Voltmeter
// Module Name:   /home/lab_jos/Stachanczyk_Migas/JOS_Woltomierz-dev/Volt_TB.v
// Project Name:  woltomierz
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Voltmeter
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module Volt_TB;

	// Inputs
	reg rst;
	reg clk;

	// Outputs
	wire E;
	wire RW;
	wire RS;
	wire [7:0] db_out;
	
	wire SPI_SCK;
	wire SPI_MOSI;
	wire AMP_CS;
	wire AD_CONV;
	
	reg ADC_OUT;
	wire AMP_SHDN;

	reg ROT_A;
	reg ROT_B;
	
	// Instantiate the Unit Under Test (UUT)
	Voltmeter #(.div(5)) uut (
		.E(E), 
		.RW(RW), 
		.RS(RS), 
		.db_out(db_out), 
		.rst(rst), 
		.clk(clk),
		.SPI_SCK(SPI_SCK), .SPI_MOSI(SPI_MOSI),.AMP_CS(AMP_CS),
		 .AD_CONV(AD_CONV), .ADC_OUT(ADC_OUT), .AMP_SHDN(AMP_SHDN),
		 .ROT_A(ROT_A),.ROT_B(ROT_B)
	);

	initial begin
		// Initialize Inputs
		rst = 0;
		clk = 0;
		
		ROT_A=1'b0;
		ROT_B=1'b0;
		
		#10
		rst=1;
		#10
		rst=0;
		ADC_OUT=1;

		// Wait 100 ns for global reset to finish
		#55000;
		
		ROT_A=1'b1;
		#5000;
      $finish;
		
		
		// Add stimulus here

	end
	
	always #10 clk=~clk;
      
endmodule

