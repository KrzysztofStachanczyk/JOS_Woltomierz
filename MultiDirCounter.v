`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:24:27 11/15/2017 
// Design Name: 
// Module Name:    MultiDirCounter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module  MultiDirCounter #(parameter bits = 8)(
    en,
    up,
    out,
    rst,
    clk
    );

input en,up,rst,clk;
output reg [bits-1:0] out;

always @(posedge clk,posedge rst)
	if(rst)
		out <= 1;
	else
		begin
			if(en)
				begin
					if(up)
						out <= out+1;
					else
						out <= out-1;
				end
		end


endmodule
