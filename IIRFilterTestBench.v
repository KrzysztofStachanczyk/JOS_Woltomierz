`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Krzysztof Stachanczyk
//
// Create Date:   10:56:54 11/28/2017
// Design Name:   IIRFilter
// Module Name:   /home/krzysztof/Pulpit/JOS_Woltomierz/IIRFilterTestBench.v
// Project Name:  woltomierz
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: IIRFilter
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module IIRFilterTestBench;
	integer i;

	// Inputs
	reg [7:0] in;
	reg clk;
	reg newData;
	reg rst;

	// Outputs
	wire [7:0] out;

	// Instantiate the Unit Under Test (UUT)
	IIRFilter #(.filterCoefficientPow2(4),.dataSize(8)) uut (
		.in(in), 
		.out(out), 
		.clk(clk), 
		.newData(newData), 
		.rst(rst)
	);

	initial begin
		// Initialize Inputs
		in = 0;
		clk = 0;
		newData = 0;
		rst = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		rst = 1;
		#20;
		rst = 0; 

		in = 8'b10000000;
		
		// some ticks for begin
		for(i=0;i<64;i=i+1)
		begin
			newData=1;
			#20;
			newData=0;
			#80;
		end
		
		// generate unit step
		in = 8'b01111111;
		
		// look at response
		for(i=0;i<64;i=i+1)
		begin
			newData=1;
			#20;
			newData=0;
			#80;
		end
		
		
		// generate unit step
		in = 8'b10000000;
		
		// look at response
		for(i=0;i<16;i=i+1)
		begin
			newData=1;
			#20;
			newData=0;
			#80;
		end
		
		
		
		// reset and test filter initialization
		rst = 1;
		#20;
		rst = 0;
		
		in = 8'hAA;
		
		newData=1;
		#20;
		newData=0;
		#80;
		
		#200;
		
		$finish;
	end
		
	always #10 clk=~clk;
endmodule

