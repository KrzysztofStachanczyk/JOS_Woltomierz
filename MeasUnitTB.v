`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Krzysztof Stachanczyk
//
// Create Date:   12:59:01 11/28/2017
// Design Name:   MeasUnit
// Module Name:   /home/krzysztof/Pulpit/JOS_Woltomierz/MeasUnitTB.v
// Project Name:  woltomierz
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: MeasUnit
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module MeasUnitTB;

	// Inputs
	reg clk;
	reg rst;
	reg ADC_OUT;

	// Outputs
	wire [13:0] chAFiltered;
	wire [13:0] chBFiltered;
	wire SPI_SCK;
	wire SPI_MOSI;
	wire AMP_CS;
	wire AD_CONV;
	wire AMP_SHDN;
	reg updateAmplifierConf;

	
	// Instantiate the Unit Under Test (UUT)
	MeasUnit uut (
		.clk(clk), 
		.rst(rst), 
		.chAFiltered(chAFiltered), 
		.chBFiltered(chBFiltered), 
		.SPI_SCK(SPI_SCK), 
		.SPI_MOSI(SPI_MOSI), 
		.AMP_CS(AMP_CS), 
		.AD_CONV(AD_CONV), 
		.ADC_OUT(ADC_OUT),
		.updateAmplifierConf(updateAmplifierConf),
		.AMP_SHDN(AMP_SHDN)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		rst = 0;
		ADC_OUT = 1;
		updateAmplifierConf=0;

		// Wait 100 ns for global reset to finish
		#100;
        
		rst=1;
		#20;
		rst=0;
		
		#3000;
		
		updateAmplifierConf=1;
		#20;
		updateAmplifierConf=0;
		
		#50000;
		$finish;
		
		// Add stimulus here

	end
   
	always #10 clk=~clk;
endmodule

