`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:07:15 10/05/2017 
// Design Name: 
// Module Name:    ClockDivider 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ClockDivider #(parameter div = 25000000)(
    input clk,
    input rst,
    output reg clk_slow
    );

	localparam bits = clogb2(div);
	//localparam bits = 1;
	
	reg [bits-1:0] cnt;
	
	/*
		definicja przypisania ciaglego - wezla
	*/
	assign comp = (cnt==div-1);
	
	always @(posedge clk,posedge rst)
		if(rst)
			cnt <= {bits{1'b0}};
		else if ( comp )
			cnt <= {bits{1'b0}};
		else 
			cnt <= cnt +1; 
			

	always @(posedge clk,posedge rst)
		if(rst)
			clk_slow = 1'b0;
		else if(comp)
			clk_slow = ~clk_slow;
			
			
	function integer clogb2;
			input [31:0] value;
			begin
				value = value - 1;
				for (clogb2=0; value>0 ; clogb2 = clogb2 +1)
					value = value >> 1;
			end
	endfunction
	
endmodule
