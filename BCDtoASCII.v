`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Krzysztof Stachanczyk
// 
// Create Date:    14:27:56 11/16/2017 
// Design Name: 
// Module Name:    BCDtoASCII 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//		Simple module for BCD to ASCII conversion
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module BCDtoASCII(
    input [3:0] in,
    output reg [7:0] out
    );

always @*
begin
	out = in + 8'd48;
end

endmodule
