`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Krzysztof Stachanczyk
// 
// Create Date:    11:28:00 11/28/2017 
// Design Name: 
// Module Name:    MeasUnit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MeasUnit(
		input clk,
		input rst,
		
		output [13:0] chAFiltered,
		output [13:0] chBFiltered,
		
		output [3:0] commonChGain,

		output SPI_SCK,
		output SPI_MOSI,
		output AMP_CS,
		output AD_CONV,
		output AMP_SHDN,
		
		input ADC_OUT,
		
		input rotorA,
		input rotorB
		
		
    );
/*
NET "SPI_MOSI" LOC = "AB14" | IOSTANDARD = LVCMOS33 | DRIVE = 8 | SLEW = SLOW ;
NET "AMP_CS" LOC = "W6" | IOSTANDARD = LVCMOS33 | DRIVE = 8 | SLEW = SLOW ;
NET "SPI_SCK" LOC = "AA20" | IOSTANDARD = LVCMOS33 | DRIVE = 8 | SLEW = SLOW ;

NET "AMP_SHDN" LOC = "W15" | IOSTANDARD = LVCMOS33 | DRIVE = 8 | SLEW = SLOW ;

NET "AD_CONV" LOC = "Y6" | IOSTANDARD = LVCMOS33 | DRIVE = 8 | SLEW = SLOW ;
NET "ADC_OUT" LOC = "D16" | IOSTANDARD = LVCMOS33  ;
*/
wire amplifierFinish;
wire adcReadReady;
wire trigADCRead;
wire trigAmplifierWrite;

wire [13:0] chA;
wire [13:0] chB;

wire LTC6912_spiClk;
wire LTC1407_spiClk;

wire gainChanged;

assign SPI_SCK = LTC6912_spiClk | LTC1407_spiClk;


AmpGainRotor gainRotor(.rotorA(rotorA),.rotorB(rotorB),.clk(clk),.rst(rst),.gain(commonChGain),.gainChanged(gainChanged));

VoltmeterMeasFSM #(.measureDelay(50000)) fsm(.clk(clk),.rst(rst),.amplifierFinish(amplifierFinish),.adcReadReady(adcReadReady),
	.updateAmplifierConf(gainChanged),.trigADCRead(trigADCRead),.trigAmplifierWrite(trigAmplifierWrite));

IIRFilter #(.filterCoefficientPow2(6),.dataSize(14)) chAFilter(.in(chA),.out(chAFiltered),.clk(clk),.newData(adcReadReady),.rst(rst));
IIRFilter #(.filterCoefficientPow2(6),.dataSize(14)) chBFilter(.in(chB),.out(chBFiltered),.clk(clk),.newData(adcReadReady),.rst(rst));

LTC6912Driver amplifierDriver(.clk(clk),.rst(rst),.en(trigAmplifierWrite),.clr_ctrl(1'b1),.chAGain(commonChGain),.chBGain(commonChGain),	
	 .sclk(LTC6912_spiClk),.mosi(SPI_MOSI),.ss(AMP_CS),.finish(amplifierFinish),.clr(AMP_SHDN));

LTC1407Driver adcDriver(.sclk(LTC1407_spiClk),.en(trigADCRead),.clk(clk),.rst(rst),.finish(adcReadReady),.miso(ADC_OUT),.conv(AD_CONV),.chA(chA),.chB(chB));

endmodule
