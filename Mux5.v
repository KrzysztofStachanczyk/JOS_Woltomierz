`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:35:13 11/23/2017 
// Design Name: 
// Module Name:    Mux5 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Mux5 #(parameter SIZE=4)(
    input [2:0] adr,
	 
	 input [SIZE-1:0] value0,
	 input [SIZE-1:0] value1,
	 input [SIZE-1:0] value2,
	 input [SIZE-1:0] value3,
	 input [SIZE-1:0] value4,
    output reg [SIZE-1:0] out
    );

always @*
	case(adr)
		0: out<= value0;
		1: out<= value1;
		2: out<= value2;
		3: out<= value3;
		4: out<= value4;
		default: out <= 0;
	endcase
endmodule